export function isValidData(variable, datatype, length = null) {
  switch (datatype.toLowerCase()) {
    case "string":
      if (!variable) {
        return false;
      }
      if (length && variable.length !== length) {
        return false;
      }
      return typeof variable === "string";
    case "integer":
      let x;
      return isNaN(variable) ? !1 : ((x = parseFloat(variable)), (0 | x) === x);
    case "array":
      return Array.isArray(variable);
    case "email":
      if (!variable) {
        return false;
      }
      const emailPattern =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return emailPattern.test(String(variable).toLowerCase());
    case "phone_number":
      const phonePattern = /^\d{10}$/;
      return phonePattern.test(String(variable).toLowerCase());

    default:
      return false;
  }
}
