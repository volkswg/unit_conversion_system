export function formatSuperScript(textIn) {
  const unitSymbolHTML = textIn.replace(/[\^]([0-9]+)/g, "<sup>$1</sup>");

  const retComp = <span dangerouslySetInnerHTML={{ __html: unitSymbolHTML }}></span>;
  return retComp;
}
