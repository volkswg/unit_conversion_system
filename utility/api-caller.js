import axios from "axios";

export function apiCaller(apiPath, body) {
  return new Promise((resolve, reject) => {
    axios({
      method: "post",
      url: apiPath,
      data: body,
      headers: { "Content-Type": "application/json" },
    })
      .then((result) => {
        resolve(result.data);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
