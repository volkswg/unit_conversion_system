export function identifyError(error_obj) {
  const ret_error_obj = {
    success: false,
    code: 0,
    message: "",
  };

  ret_error_obj.code = error_obj?.code || 99;
  ret_error_obj.message = error_obj?.message || "Unknow Error";

  return ret_error_obj;
}
