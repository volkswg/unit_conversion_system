import availableFieldUnit from "../../../assets/mock-database/field-unit-available.json";
import { identifyError } from "../../../utility/error-lib";
import { isValidData } from "../../../utility/validator";

const handler = async (req, res) => {
  const response = { status: { success: false, message: "", code: -1 } };
  if (req.method !== "POST") {
    return;
  }

  const req_body = req.body;

  try {
    response.data = { measurement_field_units: availableFieldUnit };
    response.status.message = "Success";
    response.status.code = 0;
    response.status.success = true;
    res.status(200).json(response);
  } catch (error) {
    response.status = identifyError(error);
    res.status(500).json(response);
  }
};

export default handler;
