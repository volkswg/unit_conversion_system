import measurementSystemList from "../../../assets/mock-database/measurement-system.json";
import availableFieldUnit from "../../../assets/mock-database/field-unit-available.json";
import { identifyError } from "../../../utility/error-lib";
import { isValidData } from "../../../utility/validator";
import fs from "fs";

const handler = async (req, res) => {
  const response = { status: { success: false, message: "", code: -1 } };
  if (req.method !== "POST") {
    return;
  }

  const req_body = req.body;

  try {
    // console.log(req_body);
    const editingField = req_body.field;
    const [newUnit] = req_body.units;
    // console.log(editingField, newUnit);

    const currentavailableUnit = availableFieldUnit.find(
      (e) => e.field === editingField
    ).availableUnit;

    const newUnitData = {
      id: currentavailableUnit.length,
      multiplier: +newUnit.multiplier,
      symbol: newUnit.symbol,
    };
    currentavailableUnit.push(newUnitData);
    // console.log(availableFieldUnit[1]);
    fs.writeFileSync(
      "assets/mock-database/field-unit-available.json",
      JSON.stringify(availableFieldUnit)
    );

    response.status.code = 0;
    response.status.success = true;
    res.status(201).json(response);
  } catch (error) {
    response.status = identifyError(error);
    res.status(500).json(response);
  }
};

export default handler;
