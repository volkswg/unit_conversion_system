import flightData from "../../../assets/mock-database/flight-data.json";
import { identifyError } from "../../../utility/error-lib";
import { isValidData } from "../../../utility/validator";

const handler = async (req, res) => {
  const response = { status: { success: false, message: "", code: -1 } };
  if (req.method !== "POST") {
    return;
  }

  const req_body = req.body;

  try {
    const { flight_plan_id } = req_body;
    if (!isValidData(flight_plan_id, "integer")) {
      throw { message: "Invalid Input Flight Plan Id", code: 1 };
    }
    const flightPlanResult = flightData[flight_plan_id];

    response.data = { flight_plan_data: flightPlanResult };
    response.status.message = "Success";
    response.status.code = 0;
    response.status.success = true;
    res.status(200).json(response);
  } catch (error) {
    response.status = identifyError(error);
    res.status(500).json(response);
  }
};

export default handler;
