import measurementSystemList from "../../../assets/mock-database/measurement-system.json";
import availableFieldUnit from "../../../assets/mock-database/field-unit-available.json";
import { identifyError } from "../../../utility/error-lib";
import { isValidData } from "../../../utility/validator";
import fs from "fs";

const handler = async (req, res) => {
  const response = { status: { success: false, message: "", code: -1 } };
  if (req.method !== "POST") {
    return;
  }

  const req_body = req.body;

  try {
    // console.log(req_body);
    const editingMeasurementSystem = measurementSystemList[req_body.id];
    const newUnits = req_body.units;

    editingMeasurementSystem.units = newUnits;
    console.log(measurementSystemList);

    fs.writeFileSync(
      "assets/mock-database/measurement-system.json",
      JSON.stringify(measurementSystemList)
    );

    response.status.message = "Success";
    response.status.code = 0;
    response.status.success = true;
    res.status(202).json(response);
  } catch (error) {
    response.status = identifyError(error);
    res.status(500).json(response);
  }
};

export default handler;
