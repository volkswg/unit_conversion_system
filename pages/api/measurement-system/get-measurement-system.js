import measurementSystemList from "../../../assets/mock-database/measurement-system.json";
import availableFieldUnit from "../../../assets/mock-database/field-unit-available.json";
import { identifyError } from "../../../utility/error-lib";
import { isValidData } from "../../../utility/validator";

const handler = async (req, res) => {
  const response = { status: { success: false, message: "", code: -1 } };
  if (req.method !== "POST") {
    return;
  }

  const req_body = req.body;

  try {
    const { measurement_system_id } = req_body;
    if (!isValidData(measurement_system_id, "integer")) {
      throw { message: "Invalid Input Measurement System Id", code: 1 };
    }

    const repMeasurementSystem = JSON.parse(JSON.stringify(measurementSystemList))[
      measurement_system_id
    ];
    let item = repMeasurementSystem;
    // join unit data from available field unit
    const newUnitsData = [];
    for (let unit of item.units) {
      const fieldname = unit.field;
      const selectedUnitId = unit.availableUnitId;

      const selectedField = availableFieldUnit.find((e) => e.field === fieldname);

      const selectedUnitData = selectedField.availableUnit.find((e) => e.id === selectedUnitId);
      const joinedUnitData = {
        id: selectedUnitData.id,
        field: unit.field,
        displayText: selectedField.displayText,
        multiplier: selectedUnitData.multiplier,
        symbol: selectedUnitData.symbol,
      };
      newUnitsData.push(joinedUnitData);
    }
    item.units = newUnitsData;

    response.data = { measurement_system: repMeasurementSystem };
    response.status.message = "Success";
    response.status.code = 0;
    response.status.success = true;
    res.status(200).json(response);
  } catch (error) {
    response.status = identifyError(error);
    res.status(500).json(response);
  }
};

export default handler;
