import React, { useState, useEffect, useRef } from "react";
import TableDisplayData from "../../components/map/table-display-data";

import { apiCaller } from "../../utility/api-caller";
import classes from "../../styles/Map.module.css";
import ContentHoc from "../../components/general/layout/content-hoc/content-hoc";
import CustomTable from "../../components/general/table/custom-table";
import { formatSuperScript } from "../../utility/string-formater";

const MapPage = () => {
  const [measurementSystemSelected, setMeasurementSystemSelected] = useState(0);
  const [flightPlanData, setFlightPlanData] = useState({});
  const [measurementSystemList, setMeasurementSystemList] = useState([]);
  const totalAreaInputRef = useRef();
  const flightDistanceInputRef = useRef();
  const pesticideVolInputRef = useRef();
  const speedInputRef = useRef();

  useEffect(() => {
    apiCaller("/api/flight/get-flight-planning", { flight_plan_id: 0 }).then((result) => {
      if (result.status.success) {
        const data = result.data;
        const respFlightPlanData = data.flight_plan_data;
        setFlightPlanData(respFlightPlanData);
      }
    });
    apiCaller("/api/measurement-system/get-measurement-systems", {}).then((result) => {
      if (result.status.success) {
        const data = result.data;
        const respMeasurementSystemList = data.measurement_system_list;
        setMeasurementSystemList(respMeasurementSystemList);
      }
    });
  }, []);

  const changeMeasurementSystemHandler = (event) => {
    setMeasurementSystemSelected(event.target.value);
  };

  const inputNewValueHandler = (event) => {
    event.preventDefault();
    const newTotalArea = totalAreaInputRef.current.value;
    const newFlightDistance = flightDistanceInputRef.current.value;
    const newPesticideVol = pesticideVolInputRef.current.value;
    const newSpeed = speedInputRef.current.value;

    const respFlightPlanData = { ...flightPlanData };
    respFlightPlanData.totalArea = +newTotalArea;
    respFlightPlanData.flightDistance = +newFlightDistance;
    respFlightPlanData.pesticideVol = +newPesticideVol;
    respFlightPlanData.speed = +newSpeed;
    setFlightPlanData(respFlightPlanData);
  };

  // render table
  const selectedFlightData = flightPlanData;
  const measurementSystemData = measurementSystemList[measurementSystemSelected];

  return (
    <ContentHoc titleText="The Unit Conversion">
      <div key={measurementSystemSelected}>
        <div style={{ marginBottom: 30 }}>
          <form onSubmit={inputNewValueHandler}>
            <h3>Update Data</h3>
            <CustomTable headerList={["Field", "Value", "unit"]}>
              <tr>
                <td>Total Area</td>
                <td>
                  <input type="number" name="totalArea" ref={totalAreaInputRef} required />
                </td>
                <td>{formatSuperScript("m^2")}</td>
              </tr>
              <tr>
                <td>Flight Distance</td>
                <td>
                  <input
                    type="number"
                    name="flightDistance"
                    ref={flightDistanceInputRef}
                    required
                  />
                </td>
                <td>m</td>
              </tr>
              <tr>
                <td>Pesticide Volume</td>
                <td>
                  <input type="number" name="pesticideVol" ref={pesticideVolInputRef} required />
                </td>
                <td>Liter</td>
              </tr>
              <tr>
                <td>Speed</td>
                <td>
                  <input type="number" name="speed" ref={speedInputRef} required />
                </td>
                <td>m/s</td>
              </tr>
            </CustomTable>
            <input
              type={"submit"}
              value={"Update"}
              style={{
                border: 0,
                backgroundColor: "var(--mainColor)",
                color: "white",
                padding: "3px 10px",
                borderRadius: 5,
              }}
            />
          </form>
        </div>
        <div style={{ marginBottom: 30 }}>
          <div>
            <span style={{ color: "var(--mainColor)" }}>Measurement System</span>
          </div>
          <select
            name="measurementSystem"
            value={measurementSystemSelected}
            onChange={changeMeasurementSystemHandler}
          >
            {measurementSystemList ? (
              measurementSystemList.map((elem) => (
                <option key={elem.id} value={elem.id}>
                  {elem.name}
                </option>
              ))
            ) : (
              <div>loading</div>
            )}
          </select>
        </div>

        <div>
          {measurementSystemData && (
            <TableDisplayData
              measureSystemName={measurementSystemData.name}
              data={selectedFlightData}
              measureSystemData={measurementSystemData}
            />
          )}
        </div>
      </div>
    </ContentHoc>
  );
};

export default MapPage;
