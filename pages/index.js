// import Head from "next/head";
// import Image from "next/image";
import { useEffect } from "react";

import LoginCard from "../components/login/login-card";
// import styles from "../styles/Home.module.css";
import { useRouter } from "next/router";

const Home = () => {
  const router = useRouter();

  useEffect(() => {
    router.push("/map");
  }, []);
  return <LoginCard />;
};

export default Home;
