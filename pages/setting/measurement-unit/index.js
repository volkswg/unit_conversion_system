import React, { Fragment, useEffect, useState } from "react";
import Link from "next/link";
import { apiCaller } from "../../../utility/api-caller";
import ContentHoc from "../../../components/general/layout/content-hoc/content-hoc";
import CustomTable from "../../../components/general/table/custom-table";
import EyeIcon from "../../../components/general/svg-icon/eye_icon";

const UnitSetting = () => {
  const [measurementFieldUnits, setMeasurementFieldUnits] = useState([]);
  useEffect(() => {
    apiCaller("/api/measurement-unit/get-measurement-units", {}).then((result) => {
      if (result.status.success) {
        const data = result.data;
        const measurementFieldUnits = data.measurement_field_units;
        setMeasurementFieldUnits(measurementFieldUnits);
      }
    });
  }, []);

  return (
    <ContentHoc titleText="Measurement Unit Setting">
      <CustomTable headerList={["Field", "", ""]}>
        {measurementFieldUnits.map((fieldUnits) => (
          <tr>
            <td>{fieldUnits.displayText} </td>
            <td>
              <Link href={`/setting/measurement-unit/${fieldUnits.field}`}>
                <button style={{ border: 0, backgroundColor: "transparent" }}>
                  <EyeIcon style={{ color: "rgb(215 215 215)", width: 20 }} />
                </button>
              </Link>
            </td>
            <td>
              <Link href={`/setting/measurement-unit/${fieldUnits.field}/add`}>Add More Unit</Link>
            </td>
          </tr>
        ))}
      </CustomTable>
    </ContentHoc>
  );
};

export default UnitSetting;
