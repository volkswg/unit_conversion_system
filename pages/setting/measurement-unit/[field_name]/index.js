import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { apiCaller } from "../../../../utility/api-caller";
import ContentHoc from "../../../../components/general/layout/content-hoc/content-hoc";
import CustomTable from "../../../../components/general/table/custom-table";
import { formatSuperScript } from "../../../../utility/string-formater";

const FieldUnitDetail = () => {
  const router = useRouter();
  const { field_name } = router.query;
  const [selcetedFieldUnits, setSeletedFieldUnits] = useState([]);
  const [fieldDisplyText, setFieldDisplayText] = useState("");
  useEffect(() => {
    if (field_name) {
      apiCaller("/api/measurement-unit/get-measurement-units", {}).then((result) => {
        if (result.status.success) {
          const data = result.data;
          const respMeasurementUnitList = data.measurement_field_units;
          const selectedField = respMeasurementUnitList.find((e) => e.field === field_name);
          setFieldDisplayText(selectedField.displayText);
          console.log(field_name, selectedField);
          setSeletedFieldUnits(selectedField.availableUnit);
        }
      });
    }
  }, [field_name]);

  return (
    <ContentHoc
      titleText={`Field Unit Detail Page (${fieldDisplyText})`}
      backURL="/setting/measurement-unit"
    >
      <CustomTable headerList={["unit", "Multiplier"]}>
        {selcetedFieldUnits.map((e) => (
          <tr>
            <td>{formatSuperScript(e.symbol)}</td>
            <td>{e.multiplier}</td>
          </tr>
        ))}
      </CustomTable>
      <Link href={`/setting/measurement-unit/${field_name}/add`}>add more unit</Link>
    </ContentHoc>
  );
};

export default FieldUnitDetail;
