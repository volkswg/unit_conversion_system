import React, { useRef } from "react";
import { useRouter } from "next/router";
import { apiCaller } from "../../../../utility/api-caller";
import ContentHoc from "../../../../components/general/layout/content-hoc/content-hoc";

const AddMoreUnit = () => {
  const router = useRouter();
  const { field_name } = router.query;
  const symbolInputRef = useRef();
  const multiplierInputRef = useRef();

  const formSubmitHandler = (event) => {
    event.preventDefault();
    const editingFieldName = field_name;

    const symbol = symbolInputRef.current.value;
    const multiplier = multiplierInputRef.current.value;

    const reqBody = {
      field: editingFieldName,
      units: [{ symbol: symbol, multiplier: multiplier }],
    };
    apiCaller("/api/measurement-unit/add-measurement-unit", reqBody).then((result) => {
      if (result.status.success) {
        router.push(`/setting/measurement-unit/${field_name}`);
      } else {
        alert("Someting Went Wrong Please Try Again");
      }
    });
  };
  console.log(field_name);
  return (
    <ContentHoc
      titleText={`Add More Unit (${field_name})`}
      backURL={`/setting/measurement-unit/${field_name}`}
    >
      <h3></h3>
      <form onSubmit={formSubmitHandler}>
        <table style={{ borderCollapse: "separate", borderSpacing: "10px", marginBottom: 10 }}>
          <tr>
            <td>Symbol</td>
            <td>
              <input type="text" ref={symbolInputRef} />
            </td>
          </tr>
          <tr>
            <td>Multiplier</td>
            <td>
              <div>
                <input type="text" ref={multiplierInputRef} />
              </div>
              <div>
                <span>* 5 m = 0.005 km the multiplier is 0.001 </span>
              </div>
            </td>
          </tr>
        </table>
        <input
          type="submit"
          style={{
            border: 0,
            backgroundColor: "var(--mainColor)",
            color: "white",
            padding: "3px 10px",
          }}
        />
      </form>
    </ContentHoc>
  );
};

export default AddMoreUnit;
