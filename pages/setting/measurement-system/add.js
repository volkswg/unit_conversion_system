import React, { useRef, useEffect, useState, Fragment } from "react";
import { useRouter } from "next/router";
import ContentHoc from "../../../components/general/layout/content-hoc/content-hoc";
import CustomTable from "../../../components/general/table/custom-table";
import { apiCaller } from "../../../utility/api-caller";

const AddMeasurementSystem = () => {
  const newMeasurementSystemNameInputRef = useRef();
  const fieldsUnitInputRef = useRef([]);
  const [fieldAvailableUnit, setFieldAvailableUnit] = useState([]);
  const router = useRouter();

  const fields = [
    { name: "totalArea", displayText: "Total Area" },
    { name: "flightDistance", displayText: "Flight Distance" },
    { name: "pesticideVol", displayText: "Pesticide Volume" },
    { name: "speed", displayText: "Speed" },
  ];

  useEffect(() => {
    apiCaller("/api/measurement-unit/get-measurement-units", {}).then((result) => {
      if (result.status.success) {
        const data = result.data;
        const fieldAvailableUnit = data.measurement_field_units;
        setFieldAvailableUnit(fieldAvailableUnit);
      }
    });
  }, []);

  const submitHandler = (event) => {
    event.preventDefault();
    const newSystemName = newMeasurementSystemNameInputRef.current.value;
    const newSystemUnits = [];
    for (let eachRef of fieldsUnitInputRef.current) {
      newSystemUnits.push({ field: eachRef.name, availableUnitId: +eachRef.value });
    }
    apiCaller("/api/measurement-system/create-measurement-system", {
      name: newSystemName,
      units: newSystemUnits,
    }).then((result) => {
      if (result.status.success) {
        router.push("/setting/measurement-system");
      } else {
        alert("Someting Went Wrong Please Try Again");
      }
    });
  };

  return (
    <div>
      {fieldAvailableUnit.length > 0 ? (
        <ContentHoc titleText="Add Measurement System" backURL="/setting/measurement-system">
          <form onSubmit={submitHandler}>
            <div>
              <label>Measurement System Name</label>
              <br />
              <input type="text" ref={newMeasurementSystemNameInputRef} />
            </div>
            <br />
            <div>
              <h4>Setting Unit of each field</h4>
            </div>
            <CustomTable headerList={["field", "unit"]}>
              {fields.map((eField, i) => (
                <tr>
                  <td>{eField.displayText}</td>
                  <td>
                    <select name={eField.name} ref={(el) => (fieldsUnitInputRef.current[i] = el)}>
                      {fieldAvailableUnit.length > 0 &&
                        fieldAvailableUnit
                          .find((e) => e.field === eField.name)
                          .availableUnit.map((e) => <option value={e.id}>{e.symbol}</option>)}
                    </select>
                  </td>
                </tr>
              ))}
            </CustomTable>
            <br />
            <input type={"submit"} />
          </form>
        </ContentHoc>
      ) : (
        <div>Loading</div>
      )}
    </div>
  );
};

export default AddMeasurementSystem;
