import React, { Fragment, useState, useEffect } from "react";
import { useRouter } from "next/router";
import { apiCaller } from "../../../../utility/api-caller";
import { formatSuperScript } from "../../../../utility/string-formater";
import ContentHoc from "../../../../components/general/layout/content-hoc/content-hoc";
import CustomTable from "../../../../components/general/table/custom-table";
import Link from "next/link";

const MeasurementSystemDetail = () => {
  const router = useRouter();
  const { measurement_system_id } = router.query;
  const [measurementSystemList, setMeasurementSystemList] = useState([]);

  useEffect(() => {
    apiCaller("/api/measurement-system/get-measurement-systems", {}).then((result) => {
      if (result.status.success) {
        const data = result.data;
        const respMeasurementSystemList = data.measurement_system_list;
        setMeasurementSystemList(respMeasurementSystemList);
      }
    });
  }, []);

  const currentMeasurementSystemData = measurementSystemList[measurement_system_id];
  return (
    <Fragment>
      {currentMeasurementSystemData ? (
        <ContentHoc
          titleText={currentMeasurementSystemData.name}
          backURL="/setting/measurement-system"
        >
          <CustomTable headerList={["field", "multiplier", "unit"]}>
            {currentMeasurementSystemData.units.map((e, i) => {
              console.log(e);
              return (
                <tr key={i}>
                  <td>{e.displayText}</td>
                  <td>{e.multiplier}</td>
                  <td>{formatSuperScript(e.symbol)}</td>
                </tr>
              );
            })}
          </CustomTable>
          <Link href={`/setting/measurement-system/${measurement_system_id}/edit`}>Edit</Link>
        </ContentHoc>
      ) : (
        <h3>Loading</h3>
      )}
    </Fragment>
  );
};

export default MeasurementSystemDetail;
