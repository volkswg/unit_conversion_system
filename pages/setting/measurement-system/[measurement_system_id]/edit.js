import React, { useEffect, useState, useRef } from "react";
import ContentHoc from "../../../../components/general/layout/content-hoc/content-hoc";
import { useRouter } from "next/router";
import { apiCaller } from "../../../../utility/api-caller";
import CustomTable from "../../../../components/general/table/custom-table";
import { Fragment } from "react/cjs/react.production.min";

const EditMeasurementSystem = () => {
  const router = useRouter();
  const { measurement_system_id } = router.query;
  const [selectedMeasurementSystem, setSelectedMeasurementSystem] = useState(null);
  const [fieldUnitsList, setFieldUnitsList] = useState(null);

  const fieldInputRef = useRef([]);
  const unitInputRef = useRef([]);

  useEffect(() => {
    if (measurement_system_id) {
      apiCaller("/api/measurement-system/get-measurement-system", {
        measurement_system_id: +measurement_system_id,
      }).then((result) => {
        if (result.status.success) {
          const data = result.data;
          const respMeasurementSystem = data.measurement_system;
          setSelectedMeasurementSystem(respMeasurementSystem);
        }
      });

      apiCaller("/api/measurement-unit/get-measurement-units", {}).then((result) => {
        if (result.status.success) {
          const data = result.data;
          const respMeasurementFieldUnits = data.measurement_field_units;
          setFieldUnitsList(respMeasurementFieldUnits);
        }
      });
    }
  }, [measurement_system_id]);

  const submitHandler = (event) => {
    event.preventDefault();
    const repSelectedMeasurementSystem = JSON.parse(JSON.stringify(selectedMeasurementSystem));

    const newUnits = [];
    for (let unit of repSelectedMeasurementSystem.units) {
      const unitId = unit.id;
      const field = unit.field;
      newUnits.push({ field: field, availableUnitId: unitId });
    }

    const reqBody = {
      id: repSelectedMeasurementSystem.id,
      units: newUnits,
    };
    apiCaller("/api/measurement-system/edit-measurement-system", reqBody).then((result) => {
      if (result.status.success) {
        router.push(`/setting/measurement-system/${measurement_system_id}`);
      } else {
        alert("Someting Went Wrong Please Try Again");
      }
    });
  };

  const changeUnitHandler = (changingFieldIndex) => {
    const repSelectedMeasurementSystem = JSON.parse(JSON.stringify(selectedMeasurementSystem));
    const newUnitId = unitInputRef.current[changingFieldIndex].value;
    const changingUnit = repSelectedMeasurementSystem.units[changingFieldIndex];
    changingUnit.id = +newUnitId;
    setSelectedMeasurementSystem(repSelectedMeasurementSystem);
  };

  return (
    <ContentHoc
      titleText="Edit Measurement System"
      backURL={`/setting/measurement-system/${measurement_system_id}`}
    >
      {selectedMeasurementSystem && fieldUnitsList ? (
        <Fragment>
          <h3>{selectedMeasurementSystem.name}</h3>
          <form onSubmit={submitHandler}>
            <CustomTable headerList={["Field", "Unit"]}>
              {selectedMeasurementSystem.units.map((elemField, i) => (
                <tr>
                  <td>
                    <span>{elemField.displayText}</span>
                    <input
                      type={"hidden"}
                      value={elemField.field}
                      ref={(el) => (fieldInputRef.current[i] = el)}
                    />
                  </td>
                  <td>
                    <select
                      value={elemField.id}
                      onChange={changeUnitHandler.bind(this, i)}
                      ref={(el) => (unitInputRef.current[i] = el)}
                    >
                      {fieldUnitsList
                        .find((e) => e.field === elemField.field)
                        .availableUnit.map((unit) => (
                          <option value={unit.id}>
                            {unit.symbol} ({unit.multiplier})
                          </option>
                        ))}
                    </select>
                  </td>
                </tr>
              ))}
            </CustomTable>
            <input type="submit" />
          </form>
        </Fragment>
      ) : (
        <h3>Loading</h3>
      )}
    </ContentHoc>
  );
};

export default EditMeasurementSystem;
