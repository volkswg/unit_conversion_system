import React, { useEffect, useState } from "react";
import Link from "next/link";
import { apiCaller } from "../../../utility/api-caller";
import ContentHoc from "../../../components/general/layout/content-hoc/content-hoc";
import CustomTable from "../../../components/general/table/custom-table";
import EyeIcon from "../../../components/general/svg-icon/eye_icon";

const MeasurementSystemSetting = () => {
  const [measurementSystemList, setMeasurementSystemList] = useState([]);
  useEffect(() => {
    apiCaller("/api/measurement-system/get-measurement-systems", {}).then((result) => {
      if (result.status.success) {
        const data = result.data;
        const respMeasurementSystemList = data.measurement_system_list;
        setMeasurementSystemList(respMeasurementSystemList);
      }
    });
  }, []);
  return (
    <ContentHoc titleText="Measurement System Setting">
      <CustomTable headerList={["#", "Measurement System", ""]}>
        {measurementSystemList.map((e, i) => (
          <tr>
            <td>{i + 1}</td>
            <td>{e.name}</td>
            <td>
              <Link href={`/setting/measurement-system/${e.id}`}>
                <button style={{ border: 0, backgroundColor: "transparent" }}>
                  <EyeIcon style={{ color: "rgb(215 215 215)", width: 20 }} />
                </button>
              </Link>
            </td>
          </tr>
        ))}
      </CustomTable>
      <Link href={`/setting/measurement-system/add`}>Add Measurement System</Link>
    </ContentHoc>
  );
};

export default MeasurementSystemSetting;
