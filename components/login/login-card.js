import { useRouter } from "next/router";
import React, { useRef, useState } from "react";
import TextInput from "../general/input/text-input/text-input";
import classes from "./login-card.module.css";

const LoginCard = () => {
  const router = useRouter();
  const usernameInputRef = useRef();
  const passwordInputRef = useRef();

  const [errorStatus, setErrorStatus] = useState({ error: false, message: "Error Default Text" });

  const loginSubmitHandler = (event) => {
    event.preventDefault();
    console.log(usernameInputRef);
    const usernameInputVal = usernameInputRef.current.value;
    const passwordInputVal = passwordInputRef.current.value;
    // console.log(usernameInputRef.current.value, passwordInputRef);
    console.log(usernameInputVal, passwordInputVal);
    if (usernameInputVal === "user1" && passwordInputVal === "password") {
      console.log("logged in");
      setErrorStatus({ error: false, message: "" });
      router.push("/map");
    } else {
      // console.log("wrong username or password");
      setErrorStatus({ error: true, message: "wrong username or password" });
    }
  };

  return (
    <div style={{ display: "flex" }}>
      <div className={classes.LoginCardContainer}>
        <h3 className={classes.TitleText}>Unit Conversion System</h3>
        <br />
        <form onSubmit={loginSubmitHandler}>
          <TextInput labelText="Username" inputType="text" customRef={usernameInputRef} />
          <TextInput labelText="Password" inputType="password" customRef={passwordInputRef} />
          <span
            className={classes.ErrorText}
            style={errorStatus.error === true ? { visibility: "" } : { visibility: "hidden" }}
          >
            {errorStatus.message}
          </span>
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <input className={classes.SubmitBtn} type={"submit"} value="Sign In" />
          </div>
        </form>
      </div>
    </div>
  );
};

export default LoginCard;
