import Link from "next/link";
import React, { Fragment, useState } from "react";
import MenuIcon from "../../svg-icon/menu_icon";

import classes from "./main-header.module.css";
const MainHeader = () => {
  const [isOpenMenu, setIsOpenMenu] = useState(false);

  const openMenuHandler = () => {
    setIsOpenMenu((prevState) => !prevState);
  };
  return (
    <Fragment>
      <div className={[classes.MainHeaderContainer, isOpenMenu ? classes.Open : null].join(" ")}>
        <button className={classes.MenuIcon} onClick={openMenuHandler}>
          <MenuIcon style={{ width: 25 }} />
        </button>
        <div className={classes.MainHeaderLinkGroup}>
          <div className={classes.NavLink} onClick={openMenuHandler}>
            <Link href="/map">Unit Conversion</Link>
          </div>
          <div className={classes.NavLink} onClick={openMenuHandler}>
            <Link href="/setting/measurement-system">Measurement System</Link>
          </div>
          <div className={classes.NavLink} onClick={openMenuHandler}>
            <Link href="/setting/measurement-unit">Measurement Unit</Link>
          </div>
          <div
            className={[classes.NavLink, classes.FirstRightNavLink, classes.SignOutBtn].join(" ")}
          >
            <Link href="/" className={classes.NavLink}>
              Sign Out
            </Link>
          </div>
        </div>
      </div>
      <div className={classes.HeaderReservedSpace}></div>
    </Fragment>
  );
};

export default MainHeader;
