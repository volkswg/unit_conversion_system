// import { Fragment } from "react";
import { useRouter } from "next/router";
import { Container } from "reactstrap";
import MainHeader from "./main-header/main-header";
// import MainHeader from "./main-header/main-header";
const Layout = (props) => {
  const router = useRouter();
  const currentPathname = router.pathname;
  return (
    <div
      style={{
        backgroundColor: "rgb(253,253,253)",
        minHeight: "100vh",
        position: "relative",
      }}
    >
      {currentPathname === "/" ? null : <MainHeader />}
      <Container>
        <main>{props.children}</main>
      </Container>
    </div>
  );
};

export default Layout;
