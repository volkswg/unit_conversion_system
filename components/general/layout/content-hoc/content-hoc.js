import React from "react";
import classes from "./content-hoc.module.css";
import { useRouter } from "next/router";
/**
 * @param {{titleText: string, backURL: string}} props
 */
const ContentHoc = (props) => {
  const { titleText, backURL } = props;
  const router = useRouter();

  const clickBackHandler = () => {
    router.push(backURL);
  };

  return (
    <div className={classes.ContentHOCContainer}>
      <span
        className={[classes.BackBtn, backURL ? null : classes.HideLink].join(" ")}
        onClick={clickBackHandler}
      >
        {"< Back"}
      </span>
      <h1>{titleText}</h1>
      {props.children}
    </div>
  );
};

export default ContentHoc;
