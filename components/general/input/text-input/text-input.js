import classes from "../input.module.css";
/**
 * @param {{id: string, labelText: string, inputType: string, defaultValue: string, disabled: boolean, autoComplete: string, customRef: ref, maxLength: number, isRequire: boolean}} props
 */
const TextInput = (props) => {
  const {
    id,
    labelText,
    inputType,
    defaultValue,
    disabled,
    autoComplete,
    customRef,
    maxLength,
    isRequire,
  } = props;
  return (
    <div className={classes.InputGroup}>
      <label htmlFor={id}>{labelText}</label>
      <br />
      <input
        type={inputType}
        id={id}
        defaultValue={defaultValue}
        disabled={disabled}
        autoComplete={autoComplete}
        ref={customRef}
        maxLength={maxLength || "128"}
        required={isRequire}
      />
    </div>
  );
};

export default TextInput;
