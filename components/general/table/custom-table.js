import React from "react";
import classes from "./custom-table.module.css";
/**
 * @param {{headerList: Array}} props
 */
const CustomTable = (props) => {
  const { children, headerList } = props;
  return (
    <table className={classes.TableStyle}>
      <thead>
        <tr>
          {headerList.map((header, index) => (
            <th key={index}>{header}</th>
          ))}
        </tr>
      </thead>
      <tbody>{children}</tbody>
    </table>
  );
};

export default CustomTable;
