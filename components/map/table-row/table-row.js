import React, { useState, useEffect } from "react";
import { Fragment } from "react/cjs/react.production.min";
import { formatSuperScript } from "../../../utility/string-formater";
import PencilIcon from "../../general/svg-icon/pencil_icon";
import XMarkIcon from "../../general/svg-icon/xmark_icon";

const TableRow = (props) => {
  const { fieldName, value, symbol, fieldAvailableUnit, currentUnitId, valueMultiplier } = props;

  const [isEditUnit, setIsEditUnit] = useState(false);
  const [unitData, setUnitData] = useState({
    id: currentUnitId,
    symbol: symbol,
    multiplier: valueMultiplier,
  });

  // calculate new props
  useEffect(() => {
    setUnitData({
      id: currentUnitId,
      symbol: symbol,
      multiplier: valueMultiplier,
    });
  }, [symbol, currentUnitId, valueMultiplier]);

  const avtiveChangeUnitHandler = () => {
    setIsEditUnit((prevState) => !prevState);
  };

  const changeUnitHandler = (event) => {
    const newSelectedUnit = event.target.value;
    const newUnitData = fieldAvailableUnit[newSelectedUnit];
    setUnitData({
      id: newUnitData.id,
      symbol: newUnitData.symbol,
      multiplier: newUnitData.multiplier,
    });
    setIsEditUnit(false);
  };

  return (
    <tr>
      <td>{fieldName}</td>
      <td>{value * unitData.multiplier}</td>
      <td>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          {!isEditUnit ? (
            <Fragment>
              <span>{formatSuperScript(unitData.symbol)}</span>
              <button
                onClick={avtiveChangeUnitHandler}
                style={{ backgroundColor: "transparent", border: "none" }}
              >
                <PencilIcon style={{ width: 20, color: "#d7d7d7" }} />
              </button>
            </Fragment>
          ) : (
            <Fragment>
              <select value={unitData.id} onChange={changeUnitHandler}>
                {fieldAvailableUnit.map((e) => (
                  <option key={e.id} value={e.id}>
                    {e.symbol}
                  </option>
                ))}
              </select>
              <button
                onClick={avtiveChangeUnitHandler}
                style={{ backgroundColor: "transparent", border: "none" }}
              >
                <XMarkIcon style={{ width: 20, color: "red" }} />
              </button>
            </Fragment>
          )}
        </div>
      </td>
    </tr>
  );
};

export default TableRow;
