import React, { useState, useEffect } from "react";
import TableRow from "./table-row/table-row";
import { apiCaller } from "../../utility/api-caller";
import CustomTable from "../general/table/custom-table";

const TableDisplayData = (props) => {
  const { measureSystemName, data, measureSystemData } = props;
  const [tableBody, setTableBody] = useState([]);
  const [fieldAvailableUnit, setFieldAvailableUnit] = useState([]);

  useEffect(() => {
    apiCaller("/api/measurement-unit/get-measurement-units", {}).then((result) => {
      if (result.status.success) {
        const data = result.data;
        const fieldAvailableUnit = data.measurement_field_units;
        setFieldAvailableUnit(fieldAvailableUnit);
      }
    });
  }, []);

  useEffect(() => {
    if (fieldAvailableUnit.length > 0) {
      let buildTableBody = [];
      for (let [index, key] of Object.keys(data).entries()) {
        if (key === "id") continue;
        const fieldInfo = measureSystemData.units.find((e) => e.field === key);
        const availableUnit = fieldAvailableUnit.find((e) => e.field === key).availableUnit;

        const rawValue = data[key];
        const valueMultiplier = fieldInfo.multiplier;
        const unitSymbol = fieldInfo.symbol;

        buildTableBody.push(
          <TableRow
            key={index}
            fieldName={fieldInfo.displayText}
            value={rawValue}
            valueMultiplier={valueMultiplier}
            symbol={unitSymbol}
            currentUnitId={fieldInfo.id}
            fieldAvailableUnit={availableUnit}
          />
        );
      }
      setTableBody(buildTableBody);
    }
  }, [measureSystemData, data, fieldAvailableUnit]);

  return <CustomTable headerList={["Field", "Value", "Unit"]}>{tableBody}</CustomTable>;
};

export default TableDisplayData;
